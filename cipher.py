class Cipher:
    alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
    
    def __init__(self, key):
        if type(key) != type(str()):
            raise ValueError("The key must be of alphabetic value only")

        self.key = key.lower()

    def encrypt(self, string):
        string = string.lower()
        final = ''
        key = list(self.key)
        counter = 0
        length = len(key)

        for letter in string:
            if letter not in self.alphabet:
                final += letter
                continue
            else:
                shift = self.alphabet.index(key[counter]) + self.alphabet.index(letter)
                
                if shift >= len(self.alphabet):
                    shift = shift - len(self.alphabet)

                final += self.alphabet[shift]

                counter = counter + 1
                if counter == length:
                    counter = 0

        return final

    def decrypt(self, string):
        return "Sawa"